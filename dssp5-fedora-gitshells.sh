# SPDX-FileCopyrightText: © 2021 Dominick Grift <dominick.grift@defensec.nl>
# SPDX-License-Identifier: Unlicense

#!/usr/bin/env bash

# Created by argbash-init v2.8.1
# Rearrange the order of options below according to what you would like to see in the help message.
# ARG_OPTIONAL_SINGLE([username],[u],[User name for dssp5-fedora git shell])
# ARG_OPTIONAL_SINGLE([password],[p],[Password for dssp5-fedora git shell],[abc123])
# ARG_OPTIONAL_BOOLEAN([delete],[d],[Remove dssp5-fedora git shell])
# ARGBASH_SET_INDENT([    ])
# ARGBASH_SET_DELIM([ =])
# ARG_OPTION_STACKING([getopt])
# ARG_RESTRICT_VALUES([no-local-options])
# ARG_VERSION_AUTO([1.0.0])
# ARG_VERBOSE([])
# ARG_HELP([Configure dssp5-fedora git shells],[I use this script to configure dssp5-fedora git shells])
# ARGBASH_GO()
# needed because of Argbash --> m4_ignore([
### START OF CODE GENERATED BY Argbash v2.10.0 one line above ###
# Argbash is a bash code generator used to get arguments parsing right.
# Argbash is FREE SOFTWARE, see https://argbash.io for more info


die()
{
    local _ret="${2:-1}"
    test "${_PRINT_HELP:-no}" = yes && print_help >&2
    echo "$1" >&2
    exit "${_ret}"
}


evaluate_strictness()
{
    [[ "$2" =~ ^-(-(username|password|delete|version|verbose|help)$|[updvh]) ]] && die "You have passed '$2' as a value of argument '$1', which makes it look like that you have omitted the actual value, since '$2' is an option accepted by this script. This is considered a fatal error."
}


begins_with_short_option()
{
    local first_option all_short_options='updvh'
    first_option="${1:0:1}"
    test "$all_short_options" = "${all_short_options/$first_option/}" && return 1 || return 0
}

# THE DEFAULTS INITIALIZATION - OPTIONALS
_arg_username=
_arg_password="abc123"
_arg_delete="off"
_arg_verbose=0


print_help()
{
    printf '%s\n' "Configure dssp5-fedora git shells"
    printf 'Usage: %s [-u|--username <arg>] [-p|--password <arg>] [-d|--(no-)delete] [-v|--version] [--verbose] [-h|--help]\n' "$0"
    printf '\t%s\n' "-u, --username: User name for dssp5-fedora git shell (no default)"
    printf '\t%s\n' "-p, --password: Password for dssp5-fedora git shell (default: 'abc123')"
    printf '\t%s\n' "-d, --delete, --no-delete: Remove dssp5-fedora git shell (off by default)"
    printf '\t%s\n' "-v, --version: Prints version"
    printf '\t%s\n' "--verbose: Set verbose output (can be specified multiple times to increase the effect)"
    printf '\t%s\n' "-h, --help: Prints help"
    printf '\n%s\n' "I use this script to configure dssp5-fedora git shells"
}


parse_commandline()
{
    while test $# -gt 0
    do
        _key="$1"
        case "$_key" in
            -u|--username)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_username="$2"
                shift
                evaluate_strictness "$_key" "$_arg_username"
                ;;
            --username=*)
                _arg_username="${_key##--username=}"
                evaluate_strictness "$_key" "$_arg_username"
                ;;
            -u*)
                _arg_username="${_key##-u}"
                evaluate_strictness "$_key" "$_arg_username"
                ;;
            -p|--password)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_password="$2"
                shift
                evaluate_strictness "$_key" "$_arg_password"
                ;;
            --password=*)
                _arg_password="${_key##--password=}"
                evaluate_strictness "$_key" "$_arg_password"
                ;;
            -p*)
                _arg_password="${_key##-p}"
                evaluate_strictness "$_key" "$_arg_password"
                ;;
            -d|--no-delete|--delete)
                _arg_delete="on"
                test "${1:0:5}" = "--no-" && _arg_delete="off"
                ;;
            -d*)
                _arg_delete="on"
                _next="${_key##-d}"
                if test -n "$_next" -a "$_next" != "$_key"
                then
                    { begins_with_short_option "$_next" && shift && set -- "-d" "-${_next}" "$@"; } || die "The short option '$_key' can't be decomposed to ${_key:0:2} and -${_key:2}, because ${_key:0:2} doesn't accept value and '-${_key:2:1}' doesn't correspond to a short option."
                fi
                ;;
            -v|--version)
                printf '%s %s\n\n%s\n' "dssp5-fedora-gitshells.sh" "1.0.0" ''
                exit 0
                ;;
            -v*)
                printf '%s %s\n\n%s\n' "dssp5-fedora-gitshells.sh" "1.0.0" ''
                exit 0
                ;;
            --verbose)
                _arg_verbose=$((_arg_verbose + 1))
                ;;
            -h|--help)
                print_help
                exit 0
                ;;
            -h*)
                print_help
                exit 0
                ;;
            *)
                _PRINT_HELP=yes die "FATAL ERROR: Got an unexpected argument '$1'" 1
                ;;
        esac
        shift
    done
}

parse_commandline "$@"

# OTHER STUFF GENERATED BY Argbash

### END OF CODE GENERATED BY Argbash (sortof) ### ])
# [ <-- needed because of Argbash

test -z $_arg_username && die "user name must be specified"

if [[ $_arg_delete == "off" ]]
then

    getent passwd $_arg_username >/dev/null && die "add user failed"

    retval=0
    getent group dssp5-fedora-gitshared >/dev/null || \
	groupadd dssp5-fedora-gitshared
    retval=$?

    test $retval -eq 0 || die "add user failed"

    retval=0
    getent group dssp5-fedora-gitshells >/dev/null || \
	groupadd dssp5-fedora-gitshells
    retval=$?

    test $retval -eq 0 || die "add user failed"

    useradd -s /usr/bin/git-shell \
	    -G dssp5-fedora-gitshared,dssp5-fedora-gitshells $_arg_username || \
	die "failed to add user"

    retval=0
    echo "$_arg_password" | passwd $_arg_username --stdin
    retval=$?

    test $retval -eq 0 || die "add user failed"

    systemctl mask user@$(id -u $_arg_username).service \
	      user-runtime-dir@$(id -u $_arg_username).service || \
	die "add user failed"

    systemctl daemon-reload || die "add user failed"

    setfacl -m g:dssp5-fedora-gitshared:x \
	    $(grep $_arg_username /etc/passwd | awk -F":" '{ print $6 }') || \
	die "add user failed"

    mkdir $(grep $_arg_username /etc/passwd \
		| awk -F":" '{ print $6 }')/public_git || \
	die "add user failed"

    mkdir $(grep $_arg_username /etc/passwd \
		| awk -F":" '{ print $6 }')/public_git/${_arg_username}.git || \
	die "add user failed"

    git --git-dir=$(grep $_arg_username /etc/passwd \
			| awk -F":" '{ print $6 }')/public_git/${_arg_username}.git \
	--bare init --shared || \
	die "add user failed"

    test -x /usr/bin/htpasswd && \
	{ htpasswd -bc $(grep $_arg_username /etc/passwd \
			     | awk -F":" '{ print $6 }')/public_git/${_arg_username}.git/.htpasswd \
		   $_arg_username "$_arg_password" || \
	      die "add user failed" ;}

    mkdir $(grep $_arg_username /etc/passwd \
		| awk -F":" '{ print $6 }')/git-shell-commands || \
	die "add user failed"

    retval=0
    cat > $(grep $_arg_username /etc/passwd \
		| awk -F":" '{ print $6 }')/git-shell-commands/help <<EOF
#!/bin/sh
if tty -s
then
     echo "Run 'help' for help, or 'exit' to leave.  Available commands:"
else
     echo "Run 'help' for help.  Available commands:"
fi
cd "\$(dirname "\$0")"
for cmd in *
do
     case "\$cmd" in
     help) ;;
     *) [ -f "\$cmd" ] && [ -x "\$cmd" ] && echo "\$cmd" ;;
     esac
done
EOF
    retval=$?

    test $retval -eq 0 || die "add user failed"

    chmod +x $(grep $_arg_username /etc/passwd \
		   | awk -F":" '{ print $6 }')/git-shell-commands/help || \
	die "add user failed"

    retval=0
    cat > $(grep $_arg_username /etc/passwd \
		| awk -F":" '{ print $6 }')/public_git/${_arg_username}.git/hooks/update <<EOF
#!/bin/bash
COMMITTER=\$(/usr/bin/id -u -n)
if [[ ! "\$COMMITTER" == "${_arg_username}" && ! "\$COMMITTER" == "apache" ]]
then
     echo "Access Denied for \$COMMITTER"
     exit 1
fi
EOF
        retval=$?

        test $retval -eq 0 || die "add userfailed"

	chmod +x $(grep $_arg_username /etc/passwd \
		       | awk -F":" '{ print $6 }')/public_git/${_arg_username}.git/hooks/update || \
	die "add user failed failed"

	chown -R ${_arg_username}.${_arg_username} \
	      $(grep $_arg_username /etc/passwd | awk -F":" '{ print $6 }') || \
	    die "add user failed"

	chown -R ${_arg_username}.dssp5-fedora-gitshared \
	      $(grep $_arg_username /etc/passwd \
		    | awk -F":" '{ print $6 }')/public_git || \
	    die "add user failed failed"

	test -f $(grep $_arg_username /etc/passwd \
		      | awk -F":" '{ print $6 }')/public_git/${_arg_username}.git/.htpasswd && \
	    { chown -R ${_arg_username}.dssp5-fedora-gitshared \
		    $(grep $_arg_username /etc/passwd \
			  | awk -F":" '{ print $6 }')/public_git/${_arg_username}.git/.htpasswd || \
		  die "add user failed" ;}

	if semodule -l | grep ^"dssp5-fedora-gitshells"$ >/dev/null
	then

            semodule -nBP || die "add user failed"

            restorecon -RF "$(getent passwd $_arg_username | cut -d: -f6)" || \
		die "add user failed"

            restorecon -RF "/var/spool/mail/${_arg_username}" || \
                die "add user failed"

	    chcon -R -t user.git.content.file \
		  $(grep $_arg_username /etc/passwd \
			| awk -F":" '{ print $6 }')/public_git/${_arg_username}.git || \
		die "add user failed"

            test -f $(grep $_arg_username /etc/passwd \
			  | awk -F":" '{ print $6 }')/public_git/${_arg_username}.git/.htpasswd && \
		{ chcon -t user.git.content.htaccess.file \
			$(grep $_arg_username /etc/passwd \
			      | awk -F":" '{ print $6 }')/public_git/${_arg_username}.git/.htpasswd || \
		      die "add use failed" ;}

            test -f /etc/selinux/dssp5-fedora/contexts/users/gitshell.id || \
		{ echo "sys.role:openssh.server.subj:s0 gitshell.role:gitshell.openssh.subj:s0" > \
                       /etc/selinux/dssp5-fedora/contexts/users/gitshell.id || \
                      die "add user failed" ;}

            grep gitshell.id$ /etc/security/sepermit.conf >/dev/null || \
		{ echo "%gitshell.id" >> /etc/security/sepermit.conf || \
                      die "add user failed" ;}

            grep gitshell.role:gitshell.subj$ /etc/selinux/dssp5-fedora/contexts/default_type >/dev/null || \
		{ echo "gitshell.role:gitshell.subj" >> \
                       /etc/selinux/dssp5-fedora/contexts/default_type || \
                      die "add user failed" ;}

	else

            test -f dssp5-fedora-gitshells.cil && die "add user failed"

            retval=0
            cat > dssp5-fedora-gitshells.cil <<EOF
(selinuxuser %dssp5-fedora-gitshells gitshell.id (systemlow systemlow))
EOF
            retval=$?

            test $retval -eq 0 || die "add user failed"

            semodule -i dssp5-fedora-gitshells.cil || \
		die "add user failed"

            restorecon -RF "$(getent passwd $_arg_username | cut -d: -f6)" || \
		die "add user failed"

            restorecon -RF "/var/spool/mail/${_arg_username}" || \
                die "add user failed"

	    chcon -R -t user.git.content.file \
		  $(grep $_arg_username /etc/passwd \
			| awk -F":" '{ print $6 }')/public_git/${_arg_username}.git || \
		die "add user failed"

            test -f $(grep $_arg_username /etc/passwd \
			  | awk -F":" '{ print $6 }')/public_git/${_arg_username}.git/.htpasswd && \

		{ chcon -t user.git.content.htaccess.file \
			$(grep $_arg_username /etc/passwd \
			      | awk -F":" '{ print $6 }')/public_git/${_arg_username}.git/.htpasswd || \
		      die "add user failed" ;}

	    \rm -f -- dssp5-fedora-gitshells.cil || \
		die "add user failed"

            test -f /etc/selinux/dssp5-fedora/contexts/users/gitshell.id || \
                { echo "sys.role:openssh.server.subj:s0 gitshell.role:gitshell.openssh.subj:s0" > \
                       /etc/selinux/dssp5-fedora/contexts/users/gitshell.id || \
                      die "add userfailed" ;}

            grep gitshell.id$ /etc/security/sepermit.conf >/dev/null || \
		{ echo "%gitshell.id" >> /etc/security/sepermit.conf || \
                      die "add user failed" ;}

            grep gitshell.role:gitshell.subj$ \
		 /etc/selinux/dssp5-fedora/contexts/default_type >/dev/null || \
		{ echo "gitshell.role:gitshell.subj" >> \
                       /etc/selinux/dssp5-fedora/contexts/default_type || \
                  die "add user failed" ;}

	fi

	test -f /etc/ssh/sshd_config.d/09-dssp5-fedora-gitshells.conf || \
            { retval=0
              cat > /etc/ssh/sshd_config.d/09-dssp5-fedora-gitshells.conf <<EOF
Match Group dssp5-fedora-gitshells
PasswordAuthentication yes
AllowTcpForwarding no
PermitTunnel no
X11Forwarding no
EOF
              retval=$?

              test $retval -eq 0 || \
		  die "add user failed" ;}

        systemctl reload sshd || die "add user failed"

elif [[ $_arg_delete == "on" ]]
then

    getent passwd $_arg_username >/dev/null || die "delete user failed"

    systemctl unmask user@$(id -u $_arg_username).service \
              user-runtime-dir@$(id -u $_arg_username).service || \
        die "delete user failed"

    systemctl daemon-reload || die "delete user failed"

    \rm -rf -- $(getent passwd $_arg_username | cut -d: -f6) || \
        die "delete user failed"

    userdel -r $_arg_username >/dev/null || die "delete user failed"

    semodule -nBP || die "delete user failed"

fi

#EOF
# ] <-- needed because of Argbash
